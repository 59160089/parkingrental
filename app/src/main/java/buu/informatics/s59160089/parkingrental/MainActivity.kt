package buu.informatics.s59160089.parkingrental

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    var parking = listOf<Car>(Car() , Car() , Car())
    var selectSlot = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setListener()

    }
    fun setListener () {
          var slotClick = listOf<TextView>(findViewById(R.id.slot1) , findViewById(R.id.slot2) , findViewById(R.id.slot3))
        for (i in slotClick) {
            i.setOnClickListener{
                fectInfo(i)
            }
        }
        findViewById<Button>(R.id.update_button).setOnClickListener{setInfo()}
        findViewById<Button>(R.id.delete_button).setOnClickListener{deleteInfo()}
    }

    fun fectInfo(view: TextView){

        selectSlot = view.id.toString()
        //Toast.makeText(this , selectSlot , Toast.LENGTH_SHORT).show()
        checkSlot()
        when(view.id){
            R.id.slot1 -> {
                findViewById<TextView>(R.id.car_id).requestFocus()
                findViewById<TextView>(R.id.car_id).text = parking.get(0).carId
                findViewById<TextView>(R.id.user_name).text = parking.get(0).username
                findViewById<TextView>(R.id.brand).text = parking.get(0).carBrand
            }
            R.id.slot2 -> {
                findViewById<TextView>(R.id.car_id).requestFocus()
                findViewById<TextView>(R.id.car_id).text = parking.get(1).carId
                findViewById<TextView>(R.id.user_name).text = parking.get(1).username
                findViewById<TextView>(R.id.brand).text = parking.get(1).carBrand
            }
            R.id.slot3 -> {
                findViewById<TextView>(R.id.car_id).requestFocus()
                findViewById<TextView>(R.id.car_id).text = parking.get(2).carId
                findViewById<TextView>(R.id.user_name).text = parking.get(2).username
                findViewById<TextView>(R.id.brand).text = parking.get(2).carBrand
            }
        }
    }

    fun setInfo () {
        if(selected()){
            if(checkInput()){
                when (selectSlot){
                    R.id.slot1.toString() -> {
                        parking[0].carId = findViewById<TextView>(R.id.car_id).text.toString()
                        parking[0].username = findViewById<TextView>(R.id.user_name).text.toString()
                        parking[0].carBrand = findViewById<TextView>(R.id.brand).text.toString()
                    }
                    R.id.slot2.toString() -> {
                        parking[1].carId = findViewById<TextView>(R.id.car_id).text.toString()
                        parking[1].username = findViewById<TextView>(R.id.user_name).text.toString()
                        parking[1].carBrand = findViewById<TextView>(R.id.brand).text.toString()
                    }
                    R.id.slot3.toString() -> {
                        parking[2].carId = findViewById<TextView>(R.id.car_id).text.toString()
                        parking[2].username = findViewById<TextView>(R.id.user_name).text.toString()
                        parking[2].carBrand = findViewById<TextView>(R.id.brand).text.toString()
                    }
                }
                clearTextInput()
                Toast.makeText(this , "Successes " , Toast.LENGTH_SHORT).show()
                checkSlot()
                selectSlot = ""
            }else{
                Toast.makeText(this , "Please fill all in put text. " , Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this , "Please select parking slot." , Toast.LENGTH_SHORT).show()
        }
    }

    fun selected() : Boolean {
        if(selectSlot == ""){
            return false
        }else{
            return true
        }
    }

    fun clearTextInput() {
        var input = listOf<TextView>(findViewById(R.id.car_id) , findViewById(R.id.user_name) , findViewById(R.id.brand))
        for (i in input){
            i.text = ""
        }
    }

    fun checkSlot() {
       for ( i in 0..parking.size-1){
           if(parking[i].username != ""){
               if(i == 0){
                   findViewById<TextView>(R.id.slot1).text = "Full"
               }else if(i == 1) {
                   findViewById<TextView>(R.id.slot2).text = "Full"
               }else{
                   findViewById<TextView>(R.id.slot3).text = "Full"
               }
           }else{
               if(i == 0){
                   findViewById<TextView>(R.id.slot1).text = "Empty"
               }else if(i == 1) {
                   findViewById<TextView>(R.id.slot2).text = "Empty"
               }else{
                   findViewById<TextView>(R.id.slot3).text = "Empty"
               }
           }
       }
    }

    fun checkInput() : Boolean {
        var slotClick = listOf<TextView>(findViewById(R.id.car_id) , findViewById(R.id.user_name) , findViewById(R.id.brand))
        var flag = true
        for (i in slotClick){
            if(i.text.toString() == ""){
                flag = false
                break
            }
        }

     return flag
    }

    fun deleteInfo() {
        when(selectSlot){

            R.id.slot1.toString() ->{
                parking[0].carId = ""
                parking[0].username = ""
                parking[0].carBrand = ""
            }
            R.id.slot2.toString() ->{
                parking[1].carId = ""
                parking[1].username = ""
                parking[1].carBrand = ""
            }
            R.id.slot3.toString() ->{
                parking[2].carId = ""
                parking[2].username = ""
                parking[2].carBrand = ""
            }

        }
        checkSlot()
        clearTextInput()
    }


}
